
angular.module('starter.controllers', ['ngCordova'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, inDev, $http) {

	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	//$scope.$on('$ionicView.enter', function(e) {
	//});

	// Form data for the login modal
	$scope.loginData = localStorage.getItem('logindata');
	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/login.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Triggered in the login modal to close it
	$scope.closeLogin = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.login = function() {
		$scope.modal.show();
	};

	$scope.logout = function() {
		localStorage.clear();
		localStorage.removeItem('loginData');
		location.reload();

	}

	// Perform the login action when the user submits the login form
	$scope.doLogin = function() {
		if(inDev){
			// var hash = md5.createHash(this.username + 'ImYouFather');
			var hash = 'ImYouFather';
			localStorage.setItem('logindata', hash);
			$scope.closeLogin();
			location.reload();
			console.log('Doing login', $scope.loginData);
			return 'success';
		}else{
			var data = {
				'user': this.username,
				'passwd': this.password
			}
			$http({
				method : "POST",
				url : "http://savealife.com.br/user/login",
				data: Object.toparams(data)
			}).then(function mySucces(response) {
				$scope.logged = response.data;
				localStorage.setItem('logindata', hash);
			}, function myError(response) {
				$scope.myWelcome = response.statusText;
			});
		}

		// Simulate a login delay. Remove this and replace with your login
		// code if using a login system
		$timeout(function() {
			$scope.closeLogin();
		}, 1000);
	};
})

.controller('SlideCtrl', function($scope, $ionicSlideBoxDelegate){
	$scope.options = {
		loop: true,
		effect: 'fade',
		speed: 500,
	}

})

.controller('RegistrationCrtl', function($scope, inDev, $http){
	$scope.registrate = function(){
		if(inDev){
			// var hash = md5.createHash(this.username + 'ImYouFather');
			var hash = 'ImYouFather';
			console.log(hash);
			localStorage.setItem('logindata', hash);
			$scope.closeLogin();
			return 'success';
			console.log('Doing login', $scope.loginData);
		}else{
			var data = {
				'user': this.username,
				'passwd': this.password
			}
			console.log($scope.loginData);
			console.log(loginData);
			console.log(this.loginData);
			$http({
				method : "POST",
				url : "http://savealife.com.br/user/registrate",
				data: Object.toparams(data)
			}).then(function mySucces(response) {
				$scope.logged = response.data;
				localStorage.setItem('logindata', hash);
			}, function myError(response) {
				$scope.myWelcome = response.statusText;
			});
		}
	}
})

.controller('InstitutionsCrtl', function($scope, inDev, $http){
	if(inDev){
		$scope.institutions = [
			{id: 1, thumb: 'img/logoSTACASA.png', nome: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@santacasa.gov.br', telefone: '3301-4544'},
			{id: 2, thumb: 'img/logoHemoARA.jpg', nome: 'Hemonucleo - Unesp', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@santacasa.gov.br', telefone: '3301-4544'},
			{id: 3, thumb: 'img/logoSTACASA.png', nome: 'Santa Casa', city: 'Matão', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@santacasa.gov.br', telefone: '3301-4544'},
			{id: 4, thumb: 'img/Horasdavidalogo.jpg', nome: 'Instituto Horas da Vida', city: 'São Paulo', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@horasdavida.org.br', telefone: '11 33231587', website: 'http://www.horasdavida.org.br/?gclid=Cj0KEQjw2fLGBRDopP-vg7PLgvsBEiQAUOnIXOqVQYyTg9uY_kYVb6yXNt-J2vanKgbuRo4jiRNdA4UaAnPu8P8HAQ'},
			{id: 5, thumb: 'img/logo-hemovet-site.png', nome: 'Hemocentro Veterinário do HOVET', city: 'São Paulo', type: '1', address: 'Bloco 4 do HOVET, dentro da FMVZ-USP', district: ' Cidade Universitária São Paulo', cep: '05508 270', atendimento: '08:00 as 18:00', email: '(ludymoroz@gmail.com', telefone: '11 99606-0060', website: 'http://www.prosangue.sp.gov.br/home/Default.aspx'},
			{id: 6, thumb: 'img/logoProsangue.jpg', nome: 'Fundação Pró-Sangue Hemocentro de São Paulo', city: 'São Paulo', type: '1', address: 'Av. Dr. Dante Pazzanese, 500', district: ' Vila Mariana', cep: '04012-180', atendimento: '08:00 as 17:00', email: 'agendamento@prosangue.gov.br', telefone: '11 21767258', website: 'http://www.prosangue.sp.gov.br/home/Default.aspx'},
		];
	}else{
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		$cordovaGeolocation
		.getCurrentPosition(posOptions)
		.then(function (position) {
			var lat  = position.coords.latitude
			$scope.latitude = lat;
			var long = position.coords.longitude
			$scope.longitude = long;

		}, function(err) {
			// error
		});
		var watchOptions = {
			timeout : 3000,
			enableHighAccuracy: false // may cause errors if true
		};
		var watch = $cordovaGeolocation.watchPosition(watchOptions);
		watch.then(
			null,
			function(err) {
				// error
			},
			function(position) {
				var lat  = position.coords.latitude
				var long = position.coords.longitude
			});

			watch.clearWatch();
		var data = {
			long: $scope.longitude ,
			lat: $scope.latitude ,
			tipo: "10"
		}
		$http({
			method : "GET",
			url : "http://savealife.com.br/institutions/getInst",
			data: Object.toparams(data)
		}).then(function mySucces(response) {
			$scope.institutions = response.data;
		}, function myError(response) {
			$scope.message = response.statusText;
		});
	}
})

.controller('InstitutionCrtl', function($scope, $stateParams, inDev, $http){
	if(inDev){
		var institutions = [
			{id: 1, thumb: 'img/logoSTACASA.png', nome: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@santacasa.gov.br', telefone: '3301-4544'},
			{id: 2, thumb: 'img/logoHemoARA.jpg', nome: 'Hemonucleo - Unesp', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@santacasa.gov.br', telefone: '3301-4544'},
			{id: 3, thumb: 'img/logoSTACASA.png', nome: 'Santa Casa', city: 'Matão', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@santacasa.gov.br', telefone: '3301-4544'},
			{id: 4, thumb: 'img/Horasdavidalogo.jpg', nome: 'Instituto Horas da Vida', city: 'São Paulo', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', atendimento: '08:00 a 12:00 / 14:00 a 18:00', email: 'agendamento@horasdavida.org.br', telefone: '11 33231587', website: 'http://www.horasdavida.org.br/?gclid=Cj0KEQjw2fLGBRDopP-vg7PLgvsBEiQAUOnIXOqVQYyTg9uY_kYVb6yXNt-J2vanKgbuRo4jiRNdA4UaAnPu8P8HAQ'},
			{id: 5, thumb: 'img/logo-hemovet-site.png', nome: 'Hemocentro Veterinário do HOVET', city: 'São Paulo', type: '1', address: 'Bloco 4 do HOVET, dentro da FMVZ-USP', district: ' Cidade Universitária São Paulo', cep: '05508 270', atendimento: '08:00 as 18:00', email: '(ludymoroz@gmail.com', telefone: '11 99606-0060', website: 'http://www.prosangue.sp.gov.br/home/Default.aspx'},
			{id: 6, thumb: 'img/logoProsangue.jpg', nome: 'Fundação Pró-Sangue Hemocentro de São Paulo', city: 'São Paulo', type: '1', address: 'Av. Dr. Dante Pazzanese, 500', district: ' Vila Mariana', cep: '04012-180', atendimento: '08:00 as 17:00', email: 'agendamento@prosangue.gov.br', telefone: '11 21767258', website: 'http://www.prosangue.sp.gov.br/home/Default.aspx'},
		];

		for (var i = 0; i < institutions.length; i++) {
			if(institutions[i].id == $stateParams.instId){
				$scope.institution = institutions[i];
			}
		}
	}else{
		var data = {
			instID: $stateParams.instId
		}
		$http({
			method : "GET",
			url : "http://savealife.com.br/institutions/getInst",
			data: Object.toparams(data)
		}).then(function mySucces(response) {
			$scope.institution = response.data;
		}, function myError(response) {
			$scope.message = response.statusText;
		});
	}
})

.controller('PartnersCrtl', function($scope, inDev, $http){
	if(inDev){
		$scope.partners = [
			{id: 1, thumb:'img/logoPetZ.png', nome: "PetZ", city: 'São Paulo', address: 'Av. Adolfo Pinheiro, 1600', district: 'Santo Amaro', cep: '06472-010', atendimento: '08:00 as 22:00', email: '', telefone: '11 21817355', website: 'https://www.petz.com.br/'},
			{id: 2, thumb:'img/logoPetLove.svg', nome: "PetLove", city: 'São Paulo', address: 'Rua James Joule, 65/22', district: 'São Paulo', cep: '04576-080', atendimento: '08:00 as 22:00', email: '', telefone: '11 21817355', website: 'https://www.petlove.com.br/'},
		];
	}else{
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		$cordovaGeolocation
		.getCurrentPosition(posOptions)
		.then(function (position) {
			var lat  = position.coords.latitude
			$scope.latitude = lat;
			var long = position.coords.longitude
			$scope.longitude = long;

		}, function(err) {
			// error
		});
		var watchOptions = {
			timeout : 3000,
			enableHighAccuracy: false // may cause errors if true
		};
		var watch = $cordovaGeolocation.watchPosition(watchOptions);
		watch.then(
			null,
			function(err) {
				// error
			},
			function(position) {
				var lat  = position.coords.latitude
				var long = position.coords.longitude
			});

			watch.clearWatch();
		var data = {
			long: $scope.longitude ,
			lat: $scope.latitude ,
			tipo: "10"
		}
		$http({
			method : "POST",
			url : "http://savealife.com.br/partner/getPartners",
			data: Object.toparams(data)
		}).then(function mySucces(response) {
			$scope.partners = response.data;
		}, function myError(response) {
			$scope.message = response.statusText;
		});
	}
})

.controller('PartnerCrtl', function($scope, $stateParams, inDev, $http){
	if(inDev){
		var partners = [
			{id: 1, thumb:'img/logoPetZ.png', nome: "PetZ", city: 'São Paulo', address: 'Av. Adolfo Pinheiro, 1600', district: 'Santo Amaro', cep: '06472-010', atendimento: '08:00 as 22:00', email: '', telefone: '11 21817355', website: 'https://www.petz.com.br/'},
			{id: 2, thumb:'img/logoPetLove.svg', nome: "PetLove", city: 'São Paulo', address: 'Rua James Joule, 65/22', district: 'São Paulo', cep: '04576-080', atendimento: '08:00 as 22:00', email: '', telefone: '11 21817355', website: 'https://www.petlove.com.br/'},
		];

		for (var i = 0; i < partners.length; i++) {
			if(partners[i].id == $stateParams.partId){
				$scope.partner = partners[i];
			}
		}
	}else{
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		$cordovaGeolocation
		.getCurrentPosition(posOptions)
		.then(function (position) {
			var lat  = position.coords.latitude
			$scope.latitude = lat;
			var long = position.coords.longitude
			$scope.longitude = long;

		}, function(err) {
			// error
		});
		var watchOptions = {
			timeout : 3000,
			enableHighAccuracy: false // may cause errors if true
		};
		var watch = $cordovaGeolocation.watchPosition(watchOptions);
		watch.then(
			null,
			function(err) {
				// error
			},
			function(position) {
				var lat  = position.coords.latitude
				var long = position.coords.longitude
			});

			watch.clearWatch();
		var data = {
			long: $scope.longitude ,
			lat: $scope.latitude ,
			tipo: "10"
		}
		$http({
			method : "GET",
			url : "http://savealife.com.br/partner/getPart",
			data: Object.toparams(data)
		}).then(function mySucces(response) {
			$scope.partner = response.data;
		}, function myError(response) {
			$scope.message = response.statusText;
		});
	}
})

.controller('AvisosCtrl', function($scope, inDev, $http, $cordovaGeolocation) {
	//tipo 1- humano / 2- veterinário
	if(inDev){
		$scope.avisos = [
			{id: 1, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'A +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 2, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'AB +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 3, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'O +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 4, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'O -', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 5, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'Todos', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 6, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'A -', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		];
	}else{
		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		$cordovaGeolocation
		.getCurrentPosition(posOptions)
		.then(function (position) {
			var lat  = position.coords.latitude
			$scope.latitude = lat;
			var long = position.coords.longitude
			$scope.longitude = long;

		}, function(err) {
			// error
		});
		var watchOptions = {
			timeout : 3000,
			enableHighAccuracy: false // may cause errors if true
		};
		var watch = $cordovaGeolocation.watchPosition(watchOptions);
		watch.then(
			null,
			function(err) {
				// error
			},
			function(position) {
				var lat  = position.coords.latitude
				var long = position.coords.longitude
			});

			watch.clearWatch();
		var data = {
			long: $scope.longitude ,
			lat: $scope.latitude ,
			tipo: "10"
		}
		$http({
			method : "POST",
			url : "http://savealife.com.br/avisos/getAvisos",
			data: Object.toparams(data)
		}).then(function mySucces(response) {
			$scope.partner = response.data;
		}, function myError(response) {
			$scope.message = response.statusText;
		});
	}
	$scope.avisos = [
		{id: 1, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'A +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		{id: 2, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'AB +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		{id: 3, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'O +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		{id: 4, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'O -', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		{id: 5, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'Todos', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		{id: 6, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'A -', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
	];
	$scope.search = function(){
		console.log('clicou');
		var vm = this;
		vm.nome = vm.search.param.$viewValue;
		console.log(vm.nome);
		var req = {
			method: 'GET',
			url:''
		};

	}
})

.controller('AvisoCtrl', function($scope, $stateParams, inDev, $http) {

	if(inDev){
		var avisos = [
			{id: 1, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'A +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 2, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'AB +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 3, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'O +', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 4, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'O -', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 5, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'Todos', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
			{id: 6, thumb: 'img/logoSTACASA.png', institution: 'Santa Casa', city: 'Araraquara', type: '1', address: 'Rua Padre Duarte, 3000', district: 'Centro', cep: '14800-000', blodType: 'A -', atendimento: '08:00 a 12:00 / 14:00 a 18:00'},
		];
		for (var i = 0; i < avisos.length; i++) {
			if(avisos[i].id == $stateParams.avisoId){
				$scope.aviso = avisos[i];
			}
		}
	}else{
		var data = {
			avisoID: $stateParams.avisoId
		}
		$http({
			method : "GET",
			url : "http://savealife.com.br/aviso/getAviso",
			data: Object.toparams(data)
		}).then(function mySucces(response) {
			$scope.partner = response.data;
		}, function myError(response) {
			$scope.message = response.statusText;
		});
	}

});
