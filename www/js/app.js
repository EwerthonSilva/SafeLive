// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])
.constant('inDev', 1)
.run(function($ionicPlatform, $interval) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}

		function checkConnection() {
			var networkState = navigator.connection.type;
			var states = {};
			states[Connection.UNKNOWN]  = 'Unknown connection';
			states[Connection.ETHERNET] = 'Ethernet connection';
			states[Connection.WIFI]     = 'WiFi connection';
			states[Connection.CELL_2G]  = 'Cell 2G connection';
			states[Connection.CELL_3G]  = 'Cell 3G connection';
			states[Connection.CELL_4G]  = 'Cell 4G connection';
			states[Connection.CELL]     = 'Cell generic connection';
			states[Connection.NONE]     = 'No network connection';

			console.log('Connection type: ' + states[networkState]);
		}

		$interval(function(){
			checkConnection();
		}, 5000);


		document.addEventListener("offline", onOffline, false);

		function onOffline() {
			// Handle the offline event
			alert('you are offline');
		}
	});
})
.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'AppCtrl'
	})

	.state('app.search', {
		url: '/search',
		views: {
			'menuContent': {
				templateUrl: 'templates/search.html'
			}
		}
	})

	.state('app.registration', {
		url: '/registration',
		views: {
			'menuContent': {
				templateUrl: 'templates/registration.html',
				controller: 'RegistrationCrtl'
			}
		}
	})

	.state('app.institutions', {
		url: '/institutions',
		views: {
			'menuContent': {
				templateUrl: 'templates/institutions.html',
				controller: 'InstitutionsCrtl'
			}
		}
	})

	.state('app.institution', {
		url: '/institutions/:instId',
		views: {
			'menuContent': {
				templateUrl: 'templates/institution.html',
				controller: 'InstitutionCrtl'
			}
		}
	})

	.state('app.partners', {
		url: '/partners',
		views: {
			'menuContent': {
				templateUrl: 'templates/partners.html',
				controller: 'PartnersCrtl'
			}
		}
	})

	.state('app.partner', {
		url: '/partners/:partId',
		views: {
			'menuContent': {
				templateUrl: 'templates/partner.html',
				controller: 'PartnerCrtl'
			}
		}
	})

	.state('app.infoHumano', {
		url: '/infoHumano',
		views: {
			'menuContent': {
				templateUrl: 'templates/infoHumano.html',
				controller: 'SlideCtrl'
			}
		}
	})

	.state('app.infoPet', {
		url: '/infoPet',
		views: {
			'menuContent': {
				templateUrl: 'templates/infoPet.html',
				controller: 'SlideCtrl'
			}
		}
	})

	.state('app.avisos', {
		url: '/avisos',
		views: {
			'menuContent': {
				templateUrl: 'templates/avisos.html',
				controller: 'AvisosCtrl'
			}
		}
	})

	.state('app.aviso', {
		url: '/avisos/:avisoId',
		views: {
			'menuContent': {
				templateUrl: 'templates/aviso.html',
				controller: 'AvisoCtrl'
			}
		}
	});
	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/avisos');
});
